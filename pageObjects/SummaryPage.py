from selenium.webdriver.common.by import By

class SummaryPage():
    def __init__(self, driver):
        self.driver = driver


    submitButton = (By.CSS_SELECTOR, "button[type='submit']")
    alertText = (By.CSS_SELECTOR, "h5.alert-danger")


    def click_submitButton(self):
        self.driver.find_element(*self.submitButton).click()

    def get_alertText(self):
        return self.driver.find_elements(*self.alertText)