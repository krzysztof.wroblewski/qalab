from selenium.webdriver.common.by import By

class DetailsPage():
    def __init__(self, driver):
        self.driver = driver


    model = (By.CSS_SELECTOR, ".card-header")
    company = (By.CSS_SELECTOR, "h5.card-title")
    carDetails = (By.CSS_SELECTOR, "p.card-text")
    rentButton = (By.CSS_SELECTOR, "a[class='btn btn-primary']")
    pickupDate = (By.XPATH, "//h6/[1]")
    dropoffDate = (By.XPATH, "//h6/[2]")


    def get_model(self):
        return self.driver.find_element(*self.model)

    def get_company(self):
        return self.driver.find_element(*self.company)

    def get_carDetails(self):
        return self.driver.find_elements(*self.carDetails)

    def get_pickupDate(self):
        return self.driver.find_elements(*self.pickupDate)

    def get_dropoffDate(self):
        return self.driver.find_elements(*self.dropoffDate)

    def click_rentButton(self):
        self.driver.find_element(*self.rentButton).click()