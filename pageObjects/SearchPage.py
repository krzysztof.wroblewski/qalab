from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

class SearchPage():
    def __init__(self, driver):
        self.driver = driver


    country = (By.ID, "country")
    city = (By.ID, "city")
    model = (By.ID, "model")
    pickupDate = (By.ID, "pickup")
    dropoffDate = (By.ID, "dropoff")
    searchButton = (By.CSS_SELECTOR, ".btn-primary")
    alertText = (By.XPATH, "//h3[@class='alert alert-danger']")
    availableCarNames = (By.XPATH, "//tr/td[2]")
    rentButton = (By.CSS_SELECTOR,"a[class='btn btn-success']")


    def select_country(self, country):
        Select(self.driver.find_element(*SearchPage.country)).select_by_visible_text(country)

    def select_city(self, city):
        Select(self.driver.find_element(*SearchPage.city)).select_by_visible_text(city)

    def set_model(self):
        return self.driver.find_element(*SearchPage.model)

    def set_pickupDate(self):
        return self.driver.find_element(*SearchPage.pickupDate)

    def set_dropoffDate(self):
        return self.driver.find_element(*SearchPage.dropoffDate)

    def click_searchButton(self):
        self.driver.find_element(*SearchPage.searchButton).click()

    def get_alertText(self):
        return self.driver.find_element(*SearchPage.alertText)

    def get_availableCarNames(self):
        return self.driver.find_elements(*SearchPage.availableCarNames)

    def get_rentButton(self):
        return self.driver.find_elements(*SearchPage.rentButton)

    def fill_from_fields(self):
        self.select_country("Poland")
        self.select_city("Wroclaw")
        self.set_model().send_keys("Toyota Aygo")
        self.set_pickupDate().send_keys("25.09.2022")
        self.set_dropoffDate().send_keys("28.09.2022")