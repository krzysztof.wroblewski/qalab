# QA Lab

## Recruitment Task

Task related to recruitment process.

## Requirements

Chrome browser version 105 \
Linux OS

## MS Windows

In order to run test suite in Windows enviroment it may be necessary to use *requirements.txt* file to build a new virtual enviroment.

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Start

After clonning project repository you need to set correct path to chromedriver inside tests/conftest.py file. \
Virtual environment venv contains all modules needed to run the test suite. \
Tests can be executed by *pytest* command.
