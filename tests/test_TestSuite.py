from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

import pytest
import re

from pageObjects.SearchPage import SearchPage
from pageObjects.DetailsPage import DetailsPage
from pageObjects.SummaryPage import SummaryPage

@pytest.mark.usefixtures("setup")
class TestSuite():

    def test_car_filtering(self):
        searchPage = SearchPage(self.driver)

        searchPage.fill_from_fields()

        searchPage.click_searchButton()

        carNames = searchPage.get_availableCarNames()
        for name in carNames:
            assert name == "Toyota Aygo"

    def test_summary_fields_mandatory(self):
        searchPage = SearchPage(self.driver)
        detailsPage = DetailsPage(self.driver)
        summaryPage = SummaryPage(self.driver)

        alerts_expected = ["Name is required", "Last name is required", "Email is required", "Card number is required"]

        searchPage.fill_from_fields()
        searchPage.click_searchButton()
        searchPage.get_rentButton()[0].click()

        detailsPage.click_rentButton()

        summaryPage.click_submitButton()

        alerts = summaryPage.get_alertText()
        alerts_text = []
        for alert in alerts:
            alerts_text.append(alert.text)

        assert alerts_text == alerts_expected


    def test_cars_only_from_selected_location(self):
        searchPage = SearchPage(self.driver)
        detailsPage = DetailsPage(self.driver)

        country = "Poland"
        city = "Wroclaw"

        searchPage.fill_from_fields()
        searchPage.click_searchButton()

        rentButtons = searchPage.get_rentButton()

        for button in rentButtons:
            button.click()

            wait = WebDriverWait(self.driver, 8)
            wait.until(expected_conditions.url_changes)


            details = detailsPage.get_carDetails()[1].text
            locationCountry = re.sub("[^a-zA-Z\d\s]", "", details).split(" ")[1]
            locationCity = re.sub("[^a-zA-Z\d\s]", "", details).split(" ")[2]

            assert country == locationCountry and city == locationCity
            self.driver.back()
