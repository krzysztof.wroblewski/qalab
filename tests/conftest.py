import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

# Set correct path
# Version required: Chrome 105
CHROME_DRIVER = "/home/krzysztof/Repozytorium/python/qalab/chromedriver"

@pytest.fixture()
def setup(request):   
    
    service_obj = Service(CHROME_DRIVER)
    driver = webdriver.Chrome(service=service_obj)

    driver.get("http://qalab.pl.tivixlabs.com/")

    #driver.maximize_window()
    driver.implicitly_wait(4)
    request.cls.driver = driver

    yield
    driver.close()